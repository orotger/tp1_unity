﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	
	private Rigidbody _rigidBody;
	
	private int _jumpsAmount = 2;

	private int _jumpsRemaining = 2;
	
    // Start is called before the first frame update
    void Start()
    {
		_rigidBody = GetComponent<Rigidbody>();
    }
	
	void OnCollisionEnter(Collision collision)
    {
		if (collision.gameObject.CompareTag("floor")) {
			_jumpsRemaining = _jumpsAmount;
		}
	}

    // Update is called once per frame
    void Update()
    {
        float speed = 50.0f;
	float jumpForce = 200.0f;
		
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.position = transform.position + new Vector3(-0.1f ,0.0f, 0.0f) * Time.deltaTime * speed;
        }
		
        if (Input.GetKey (KeyCode.RightArrow)) {
            transform.position = transform.position + new Vector3(0.1f ,0.0f, 0.0f) * Time.deltaTime * speed;
        }

		if ((Input.GetKeyDown(KeyCode.Space))&&(_jumpsRemaining >0)) {
            	   _rigidBody.AddForce(transform.up * jumpForce);
			_jumpsRemaining--;
		}


		
    }
}
