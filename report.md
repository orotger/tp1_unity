## TP1 Unity

# Saut

Pour savoir si le joueur peut sauter (c-à-d lorsqu'il est sur le sol), on ajoute un paramètre private qui permet de savoir si le joueur est au sol qui servira de condition pour le saut.

# Sauts multiples
Cette fois ci la condition sera faites sur un compteur de sauts restants qu'on compare à un nombre de sauts possibles qui est codé en dur.

#Choix du nombre de sauts
Pour pouvoir choisir le nombre de sauts, on aurait pu faire un slider qui servira à définir le paramètre nombre de sauts possible